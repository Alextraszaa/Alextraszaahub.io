---
layout: post
title:  "公益节点"
subtitle: 'ShadowsockR-ss 免费节点，既然是免费的就不要要求太高了谢谢。'
date:   2018-12-31 12:00:00
tags:
- 公益节点


description: ''
color: 'rgb(255,90,90)'
cover: 'https://Alextraszaa.github.io/assets/GYjd2vm/sy.png'
---

> ps 如果有不懂的可以点击下方使用教程。

​	*使用教程*
- [**点我进入**](http://Alextraszaa.github.io/2018/12/30/ShadowsocksR-ss-KJ.html)

​	*联系方式*
- [**QQ群**](http://jq.qq.com/?_wv=1027&k=5OQEbpK)
- [**Telegram**](http://t.me/joinchat/LnV6-RBdkQn2ghSc7E_K2w)

## 用前须知
-  节点可能一周或更久更新一次 ，如发现无法使用请回到此处看节点信息是否更改。
​  <center>1.因国内网络特殊性，此节点不能满足所有人。</center>
​  <center>2.如果之前在别的地方下载过软件无法使用请卸载，安装我博客里的软件点击左上角自动跳转选择你自己的系统版本。（无毒放心）</center>
​  <center>3.如常时间使用突然不能使用了请重启手机。</center>
​  <center>4.遇到APP之前可以用但是突然不能用了，但还是可以出去的，请尝试卸载APP或酸酸乳。</center>
​  <center>5.如果你是手动输入节点无法使用请尝试链接和二维码。（如果三种方法都无法使用请看第1段话）</center>
​  <center>6.如果有问题要问请把问题描述清楚谢谢。</center>
​  <center>7.最后在重申一遍此节点无法满足所有人“且是公益”不要摆出一种我都用不了好卡这一类的话出现，不愿意用**可以不用**谢谢。</center>

## 节点信息

​  <center>I  P       : 66.42.33.46</center>
​  <center>端口       : 4399</center>
​  <center>密码       : xiaoyouxi</center>
​  <center>加密       : aes-256-cfb</center>
​  <center>协议       : auth_aes128_md5</center>
​  <center>混淆       : plain</center>

ssr://NjYuNDIuMzMuNDY6NDM5OTphdXRoX2FlczEyOF9tZDU6YWVzLTI1Ni1jZmI6cGxhaW46ZUdsaGIzbHZkWGhwLz9vYmZzcGFyYW09JnJlbWFya3M9NXB1MDVhU2E2SXFDNTRLNUsxRlI1NzZrT2pneU1EUTJNemczT1EmZ3JvdXA9NkstMzVZaUc1THFyNTd1WjVweUo2WnlBNkthQjU1cUU1THE2Zmc

​![](http://Alextraszaa.github.io/assets/GYjd2vm/kj2vm.png)

​ **要做一条有梦想的咸鱼~**